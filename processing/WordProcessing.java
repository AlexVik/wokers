package processing;

import processing.TextInOut;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.*;

public class WordProcessing implements TextInOut {

	@Override
	public String inputFile(String fileContents, Path input) throws IOException {
		File inputFile = new File(String.valueOf(input));
		BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(inputFile));
		if (!fileContents.isEmpty()) {
			bufferedWriter.write(fileContents);
			bufferedWriter.close();
		}
		return fileContents;
	}
	public String outputFile(Path outFile) throws IOException {
		Path file = Paths.get(String.valueOf(outFile));
		List<String> lines = Files.readAllLines(file, StandardCharsets.UTF_8);
		return String.join("\n", lines);
	}
	@Override
	public void textInfo(String text, Path filePath) throws IOException {
		Path file = Paths.get(String.valueOf(filePath));
		String words;
		words = text.replaceAll("[^(А-Яа-яA-Za-z0-9)\\s+]", "");
		String[] wordsResult = words.split("[\\s]");
		Map<String, Integer> mapWords = new HashMap<>();
		for (String word : wordsResult) {
			mapWords.put(word, mapWords.getOrDefault(word, 0) + 1);
		}
		for (Map.Entry<String, Integer> entry : mapWords.entrySet()) {
			String k = entry.getKey();
			Integer v = entry.getValue();
			try {
				String info = "\nСлово: " + "|" + k + "|" + " всречается: " + v + " раз/а";
				writingToFile(filePath, info);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		String totalInfo = "\n Всего слов в тексте: " + wordsResult.length + "\n";
		writingToFile(filePath, totalInfo);
	}
	public Path pathEntry(String message) throws IOException {
		Scanner in = new Scanner(System.in);
		System.out.println(message);
		return Paths.get(in.nextLine());
	}

	private void writingToFile(Path filePath, String info) throws IOException {
		Files.write(Paths.get(String.valueOf(filePath)), info.getBytes(), StandardOpenOption.APPEND);
	}
	public String getString(String consoleText) {
		Scanner in = new Scanner(System.in);
		System.out.println(consoleText);
		String str;
		str = in.nextLine();
		return str;
	}
}