package processing;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;

interface TextInOut {
	String inputFile(String fileContents, Path input) throws IOException;
	void textInfo(String text, Path filePath) throws IOException;
	String outputFile(Path outFile) throws IOException;
}


