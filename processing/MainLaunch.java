package processing;

import java.io.IOException;

public class MainLaunch {
    public static void main(String[] args) throws IOException {
        WordProcessing wp = new WordProcessing();
        String consoleText = "Введите текст для записи в файл: ";
        String write = "\nВведите путь к файлу для записи текста: ";
        String read = "\nВведите путь к файлу для чтения текста: ";
        String textAnalysis = "Введите путь для вывода анализа текста";

        wp.inputFile(wp.getString(consoleText),wp.pathEntry(write));
        wp.textInfo(wp.outputFile(wp.pathEntry(read)), wp.pathEntry(textAnalysis));
    }
}
